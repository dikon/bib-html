# Webseiten-Template für Bibliographien auf Basis von BibTeX

## Verwendung

Um das Template für eigene Bibliographien zu verwenden, müssen die bibliographischen Nachweise im BibTeX-Format in die Datei [`assets/data/sources.bib`](assets/data/sources.bib) eingetragen werden. Ferner empfiehlt sich die Anpassung der bislang auf das Dikon-Projekt verweisenden Angaben in der [HTML-Datei](index.html), d.h. im `<head>` die Angaben `<title>` sowie der `<meta>`-Tag mit `keywords` und im `<body>` die Titelzeile, die sich im ersten `<pre>`-Tag findet.

## Gattungen

Der in der HTML-Datei formulierte Zitierstil ermöglicht die Ausgabe der BibTeX- bzw. BibLaTeX-Gattungen `periodical`, `article`, `book`, `inbook`, `collection`, `incollection`, `proceedings`, `inproceedings`, `report`, `online`, `misc`.

## Anwendungsbeispiel

> [dikon.pages.gwdg.de/bib](https://dikon.pages.gwdg.de/bib/)

## CSS- und JS-Komponenten

> [BibtexJS](https://github.com/pcooksey/bibtex-js)
> MIT License  
> Copyright (c) 2017 Philip Cooksey

> [JQuery](https://github.com/jquery/jquery) (3.2.1)
> MIT License  
> Copyright (c) JS Foundation and other contributors

> [Moment.js](https://github.com/moment/moment) (2.22.2)
> MIT License  
> Copyright (c) JS Foundation and other contributors

> [Bootstrap](https://github.com/twbs/bootstrap) (3.3.5)
> MIT License  
> Copyright (c) 2011-2015 Twitter, Inc.
